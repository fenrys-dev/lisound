package com.fenrys.lisound

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.support.design.widget.Snackbar

import com.spotify.sdk.android.authentication.AuthenticationClient
import com.spotify.sdk.android.authentication.AuthenticationRequest
import com.spotify.sdk.android.authentication.AuthenticationResponse

class MainActivity : AppCompatActivity() {

    var CLIENT_ID: String = "c6058b95e873437aa69f4cc4836498f3"
    var REQUEST_CODE: Int = 1001
    var REDIRECT_URI: String = "lisound://callback"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<Button>(R.id.testButton)

        button.setOnClickListener({ view ->
            run {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                val builder = AuthenticationRequest.Builder(
                        this.CLIENT_ID, AuthenticationResponse.Type.TOKEN, this.REDIRECT_URI
                )

                builder.setScopes(Array(1, { "streaming" }))
                val request = builder.build()

                AuthenticationClient.openLoginActivity(this, this.REQUEST_CODE, request)
            }
        })
    }
}
